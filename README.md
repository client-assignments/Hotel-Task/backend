# HOTEL-BOOKING

- Store blog information in a database and send all blog information data
- Api endpoints for hotel lists and hotel details.

## Installation & Configuration

- Clone the repository on your local machine. After cloning the project, follow these steps:

1. Install dependencies via npm.

```bash
npm i # (for local setup)
```
2. Create an .env file with valid values that is identical to example.env.

3. To start the server

```bash
npm start
```

## .env Reference

In .env file have to set two Api key variables.
- BOOKING_API_BASE_URL : This variable stores the booking api base url.
- RAPID_API_KEY : Rapid api key.
- RAPID_API_HOST : Rapid api host.
- DATABASE_URL : Set mongoDB database URI.

## API Reference

- Please get the `Hotel-Booking.postman_collection.json` file from directory and import it in postman.

### Folder Structure

Common structure that is used in this project is as following
```
.
└── src
    └── routes
        └── module.route.js

    └── controllers
        └── module.controller.js

    └── middleware
        └── module.middleware.js

    └── helper
        └── module.helper.js

    └── services
        └── util.service.js

    └── models
        └── module.model.js

    └── uploads
```

| File                               | Usage/Description                                                                                                                                                           |
| ---------------------------------- | --------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| routes > module.route.js           | All the route files are in the routes folder with a module name.                                                                                                            |
| helper > module.helper.js          | In this folder you'll find all the helpers with helperName.helper.js .Helper functions should be defined here which will be used as third party integration for controller. |
| controllers > module.controller.js | Controller will business logic to the API or it will use helpers to get data from third party.                                                                              |
| middleware > module.middleware.js  | This is used to validate all requests that coming to server.                                                                                                                |
| models > module.model.js           | If feature will use DB then interfaces and schema for model will be defined here.                                                                                           |
| services > module.service.js       | Service file used for the function which is not related to any services.                                                                                                    |
| uploads                            | Stores uploaded images.                                                                                                                                                     |
