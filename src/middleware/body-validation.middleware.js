const { dateIsValid } = require('../services/util.service');



/**
 * Validate request that have @req-query-param arrivalDate is valid date or not
 * Validate request that have @req-query-param departureDate is valid date or not
 * Departure Date should be greater than Arrival Date
 *
 * @param req => Express request
 * @param res => Express response
 * @param next => Express next function
 * @returns {*} => returns res having error with status code or next function
 */
const arrivalAndDepartureDateValidator = (req,res,next) => {
    const { arrivalDate, departureDate} = req.query;
    let dateError = [];

    if(!arrivalDate){
        dateError.push('Enter Arrival date');
    } else if(!dateIsValid(arrivalDate)){
        dateError.push('Enter Arrival date in valid format (YYYY-MM-DD)');
    }

    if(!departureDate){
        dateError.push('Enter Departure date');
    } else if(!dateIsValid(departureDate)){
        dateError.push('Enter Departure date in valid format (YYYY-MM-DD)');
    }

    if(Date.parse(departureDate) <= Date.parse(arrivalDate) ){
        dateError.push('Departure date must be greater than arrival date');
    }

    if(dateError.length){
        return res.status(400).send({ error : dateError});
    }

    next();
};

/**
 * Validate @req-query-params hotelId is Valid Number or not
 *
 * @param req => Express request
 * @param res => Express response
 * @param next => Express next function
 * @returns {*} => returns error with status code or next function
 */
const hotelIdValidation = (req, res, next) => {
    const {hotelId} = req.query;

    if (!hotelId) {
        return res.status(400).send({error: ['Enter hotel id']});
    }

    if (typeof hotelId !== 'number' && isNaN(Number(hotelId))) {
        return res.status(400).send({error: ['Enter valid hotel id']});
    }
    req.query.hotelId = Number(hotelId);
    next();
}

module.exports = {
    arrivalAndDepartureDateValidator,
    hotelIdValidation
}
