const express = require("express");
const cors = require('cors');

require('dotenv').config();
const mainRouter = require("./routes/blogger.routes");
const propertiesRouter = require("./routes/properties.route");
require("./db/database")

const app = express();
// enable cors
app.use(
    cors({
        origin: true,
        methods: ['GET', 'POST', 'PUT', 'DELETE', 'OPTIONS', 'PATCH'],
        allowedHeaders: [
            'Origin',
            ' X-Requested-With',
            ' Content-Type',
            ' Accept ',
            ' Authorization',
            'transcript-token',
            'contract-bot-token',
            'bot-token',
            'x-access-token',
        ],
        credentials: true,
    }),
);
// parse json request body
app.use(express.json({ limit: '10mb' }));
// api routes
app.use(mainRouter);
app.use(propertiesRouter);
app.use(express.static(__dirname + "/uploads/"));

//An error handling middleware
app.use((err, req, res, next) => {
    console.log('🐞 Error Handler');

    err.statusCode = err.statusCode || 500;
    err.status = err.status || 'error';

    res.status(err.statusCode).json({
        status: err.status,
        message: err.message,
        err: err,
    });
});

module.exports = app;