/**
 * To check date is valid and in (YYYY-MM-DD) format or not
 * Return true if date is valid else return false
 *
 * @param {string} dateString => Date in type string
 * @returns {boolean} => returns true or false
 */
function dateIsValid(dateString) {
    const regex = /^\d{4}-\d{2}-\d{2}$/;

    if (dateString.match(regex) === null) {
        return false;
    }

    const date = new Date(dateString);

    const timestamp = date.getTime();

    if (typeof timestamp !== 'number' || Number.isNaN(timestamp)) {
        return false;
    }

    return date.toISOString().startsWith(dateString);
}

module.exports = { dateIsValid }