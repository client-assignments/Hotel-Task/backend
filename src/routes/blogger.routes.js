const express = require('express');
const { body, validationResult } = require('express-validator');
const { insertBlog, getAllBlog, getBlog } = require("../controller/blogger.controller");
const router = new express.Router();

/** 
 * New Blog Insert in Database.
 * 
 * @param {string} firstname
 * @param {string} lastname
 * @param {string} contury
 * @param {string} blogheader
 * @param {string} descripption
 * @param {Array} image
 * @param {Array} activity
 * @return {
 * object{
 * _id : ObjectId
 * }
 * }
 */
router.post("/api/addblog", [
    body("firstname").not().isEmpty().withMessage("firstname is required"),
    body("lastname").not().isEmpty().withMessage("lastname is required"),
    body("country").not().isEmpty().withMessage("country is required"),
    body("blogheader").not().isEmpty().withMessage("blogheader is required"),
    body("description").not().isEmpty().withMessage("description is required")],
    async (req, res) => {
        const errors = validationResult(req);
        if (!errors.isEmpty()) {
            return res.status(400).send({ errors: errors.array() });
        } else {
            try {
                const response = await insertBlog(req.body);
                res.send(response);
            } catch (e) {
                res.status(400).send(e.message);
            }
        }
    })

/** 
 * Get All Blogs From Database.
 * @return {
 * Array [
 *  Objects{
 *      _id: ObjectID,
 *       firstname: String
 *       lastname: String,
 *       country: String,
 *       blogheader: String
 *  }
 * ]
 * } - return All Blogs in one Array. 
 */
router.get("/api/blog/list", async (req, res) => {
    const allBlog = await getAllBlog();
    res.send(allBlog)
})

/**
 * Get Blog 
 * 
 * @param {string} id - Pass Blog Id.
 * @return {
 * {
 *  _id: ObjectId,
 *   firstname: String,
 *   lastname: String,
 *   country: String,
 *   blogheader: String,
 *   description: String
 *   image: Array,
 *   activity: Array[
 *       Objects{
 *           place: String,
 *           url: String
 *       }
 *   ]
 * }
 * }
 */
router.get("/api/blog/:id", async (req, res) => {
    const blog = await getBlog(req.params.id);
    res.send(blog)
})
module.exports = router;