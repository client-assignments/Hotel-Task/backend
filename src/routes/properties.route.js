const express = require('express');
const { arrivalAndDepartureDateValidator, hotelIdValidation} = require("../middleware/body-validation.middleware");
const { getPropertyList, getPropertyDetails} = require('../controller/properties.controller');
const router = new express.Router();

router.get('/api/properties/list', arrivalAndDepartureDateValidator, getPropertyList);
router.get('/api/properties/details', arrivalAndDepartureDateValidator, hotelIdValidation, getPropertyDetails);

module.exports = router;

