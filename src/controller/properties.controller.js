const {
        getPropertyListFromBookingApi,
        getPropertyDescriptionFromBookingApi,
        getPropertyRoomDetailsFromBookingAPi
} = require('../helper/booking.helper');

/**
 * Get properties list using booking.helper.js that have Booking Rapid API integration
 *  with given arrival date and departure date.
 *
 * @param req => Express request
 * @param res => Express response
 * @param next => Express next function
 */
async function getPropertyList (req, res, next) {
        try {
                const { arrivalDate, departureDate} = req.query;
                const data = await getPropertyListFromBookingApi(arrivalDate,departureDate);
                const { result } = data || {};

                if(!result?.length){
                     return res.status(404).send({error : ['no data found']});
                }

                const propertyList = result.map((property)=> {
                        const { hotel_name, address, hotel_id, country_trans, currency_code } = property || {};
                        return {
                                hotelName : hotel_name,
                                address,
                                departureDate,
                                arrivalDate,
                                currencyCode : currency_code,
                                hotelId : hotel_id,
                                countryTrans : country_trans
                        }
                });

                res.status(200).send({ data : propertyList});

        } catch (error) {
                console.trace(error);
                return next(error);
        }
}

/**
 * Get property details with given hotel id and arrival, departure date in request query params
 *  using booking.helper.js file that have 3 booking Rapid API,
 *  1) Get properties description list with given arrival date and departure date.
 *  2) Get property description with given hotel id.
 *  3) Get property room details with given arrival date, departure date and hotel id.
 *
 * @param req => Express request
 * @param res => Express response
 * @param next => Express next function
 */
async function getPropertyDetails (req, res, next) {
        try{
                const { hotelId, arrivalDate, departureDate} = req.query;
                const propertyDetailsArray = await getPropertyListFromBookingApi(arrivalDate,departureDate);
                const propertyDescriptionData = await getPropertyDescriptionFromBookingApi(hotelId);
                const propertyRoomData = await getPropertyRoomDetailsFromBookingAPi(arrivalDate, departureDate, hotelId);
                let roomsImageAndFacility = {};
                const { result } = propertyDetailsArray;

                if (!result.length) {
                        return res.status(200).send({data: []});
                }

                if (!propertyDescriptionData.length) {
                        return res.status(400).send({data: []});
                }
                if(propertyDescriptionData.message){
                        return res.status(400).send({error: ['property details not found']});
                }
                const propertyDetails = result.find(({ hotel_id}) => hotel_id === hotelId);

                if (!propertyDetails) {
                        return res.status(400).send({error: ['property details not found']});
                }

                const filteredDetails =  {
                        hotelName : propertyDetails?.hotel_name,
                        address : propertyDetails?.address,
                        departureDate,
                        arrivalDate,
                        currencyCode : propertyDetails?.currency_code,
                        hotelId :propertyDetails?.hotel_id,
                        countryTrans : propertyDetails?.country_trans
                };
                const { description} = propertyDescriptionData.find(({ descriptiontype_id}) => descriptiontype_id === 6);

                if(propertyRoomData.length){
                        const rooms = propertyRoomData[0]?.rooms;
                        if(rooms){
                                let roomNo = 1;
                                for(let room in rooms){
                                        roomsImageAndFacility[`room-${roomNo}`] = {
                                                photos : rooms[room].photos.map((roomPhotoImage) => roomPhotoImage.url_original),
                                                facilities : rooms[room].facilities.map((facility) => facility.name)
                                        }
                                        roomNo+=1;
                                }
                        }
                }

                const hotelDetails = {
                        roomsImageAndFacility,
                        description ,
                        ...filteredDetails
                }
                res.status(200).send({
                        data: hotelDetails
                });

        } catch (error) {
                console.trace(error);
                return next(error);
        }
}

module.exports = { getPropertyList, getPropertyDetails }