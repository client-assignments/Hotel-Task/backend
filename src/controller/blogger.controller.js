const Blog = require("../model/blogger.model");
const fs = require("fs");
const path = require("path");
const { default: mongoose } = require("mongoose");

/**
 * Insert Blog Data in Database.
 * 
 * @param  {*} blogData - Request Body 
 * @returns - Inserted Recored Id
 */
const insertBlog = async (blogData) => {
    if (blogData.images) {
        const pathArray = await uploadImage(blogData.images)
        if (pathArray[0].hasOwnProperty("message")) {
            throw new Error(pathArray[0].message);
        }
        blogData.image = pathArray;
    }
    const blog = new Blog(blogData);
    const { _id } = await blog.save();
    return { _id };

}

/**
 * Return All Blogs List in Array of objects.
 * 
 * @returns - All Blogs List.
 */
const getAllBlog = async () => {
    try {
        const blogs = await Blog.find({}, { _id: 1, firstname: 1, lastname: 1, blogheader: 1, country: 1 });
        return blogs;
    } catch (e) {
        console.log(e);
        return new Error('Something went wrong');
    }
}
/**
 * Return One Blog Using Blog Id.
 * 
 * @param {string} blogId - Requested Blog Id
 * @returns { * } - All Details of Blog.
 */
const getBlog = async (blogId) => {
    try {
        const blog = await Blog.findOne({ _id: blogId });
        return blog;
    } catch (e) {
        console.log(e);
        return new Error('Something went wrong');
    }
}
/**
 * This Function is Convert Base64 Into .jpg and Store In Database If Any Error Then Throw Error.
 * 
 * @param {string} base64 - Images Base64 Array 
 * @returns {imagePath / errorArray} - If Any Error Then Return errorArray OtherWish imageArray.
 */
const uploadImage = async (base64) => {
    const uploadPath = path.join(path.dirname(__dirname), "/uploads/")
    if (!fs.existsSync(uploadPath)) {
        fs.mkdirSync(uploadPath);
    }
    const imagePath = [];
    const errorArray = [];
    await base64.forEach(async base64img => {
        const fileName = new mongoose.Types.ObjectId().toString()
        const buffer = await Buffer.from(base64img.replace(/^data:(.*,)?/, ""), "base64");
        const len = (Buffer.byteLength(buffer) / 1024);
        if (len > 2048) {
            imagePath.forEach(img => fs.unlinkSync(`${uploadPath}${img}`))
            errorArray.push({ message: `File To Large! Upload Image Under 2MB` });
            return;
        }
        imagePath.push(`${fileName}.jpg`);
        fs.writeFileSync(`${uploadPath}${fileName}.jpg`, buffer);
    });
    return (errorArray.length > 0) ? errorArray : imagePath;
}
module.exports = {
    insertBlog,
    getAllBlog,
    getBlog
}