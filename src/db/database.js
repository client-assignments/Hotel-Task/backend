const mongoose = require("mongoose");

mongoose.connect(process.env.DATABASE_URL, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
}).then(() => {
    console.log('Database Connected successfully');
})
    .catch(err => {
        console.error('DATABASE - Error');
        console.error(err);
    });