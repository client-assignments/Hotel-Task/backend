const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const bloggerSchema = new Schema({

    firstname: {
        type: Schema.Types.String,
        required: true
    },
    lastname: {
        type: Schema.Types.String,
        required: true
    },
    country: {
        type: Schema.Types.String,
        required: true
    },
    blogheader: {
        type: Schema.Types.String,
        required: true
    },
    description: {
        type: Schema.Types.String,
        required: true
    },
    image: {
        type: Schema.Types.Array,
    },
    activity: {
        type: Schema.Types.Array
    }
})

const Blog = mongoose.model("blog", bloggerSchema);

module.exports = Blog;