const axios = require("axios");

/**
 * Created Axios Instance with Booking API Base url and Rapid APi key and Rapid API Host
 *
 * @type {AxiosInstance}
 */
const instance = axios.create({
    baseURL: process.env.BOOKING_API_BASE_URL,
    headers: {
        'X-RapidAPI-Key': process.env.RAPID_API_KEY,
        'X-RapidAPI-Host': process.env.RAPID_API_HOST
    }
});

/**
 * Get properties List using Booking Rapid API with given arrival date and departure date.
 *
 * @param {string} arrivalDate - Arrival Date(YYYY-MM-DD)
 * @param {string} departureDate - Departure Date(YYYY-MM-DD)
 * @returns => returns Object of Properties details and List
 */
async function getPropertyListFromBookingApi(arrivalDate, departureDate) {
    try {
        const { data } = await instance.get('properties/list', {
            params: {
                offset: '0',
                arrival_date: arrivalDate,
                departure_date: departureDate,
                guest_qty: '1',
                dest_ids: '-3712125',
                room_qty: '1',
                search_type: 'city',
                children_qty: '2',
                children_age: '5,7',
                search_id: 'none',
                price_filter_currencycode: 'USD',
                order_by: 'popularity',
                languagecode: 'en-us',
                travel_purpose: 'leisure'
            }
        });
        return data;
    } catch (error) {
        if (error.response) {
            console.error(`Third-party api error >> ${JSON.stringify(error.response.data)}`);
            throw new Error('Something went wrong');
        } else if (error.request) {
            console.error(`Axios error >> ${JSON.stringify(error.request)}`);
            throw new Error('Something went wrong');
        } else {
            console.error(`Axios error >> ${JSON.stringify(error.message)}`);
            throw new Error('Something went wrong');
        }
    }
}

/**
 * Get Property Description using Booking Rapid API with given hotel id.
 *
 * @param {Number} hotelId - Hotel ID
 * @returns => returns array of property descriptions
 */
async function getPropertyDescriptionFromBookingApi(hotelId){
    try{
        const { data } = await instance.get('properties/get-description', {
            params: {
                hotel_ids: hotelId,
                languagecode: 'en-us',
            }
        });
        return data;
    } catch (error) {
        if (error.response) {
            console.error(`Third-party api error >> ${JSON.stringify(error.response.data)}`);
            throw new Error('Something went wrong');
        } else if (error.request) {
            console.error(`Axios error >> ${JSON.stringify(error.request)}`);
            throw new Error('Something went wrong');
        } else {
            console.error(`Axios error >> ${JSON.stringify(error.message)}`);
            throw new Error('Something went wrong');
        }
    }
}

/**
 * Get Property Room details using Booking Rapid API with given hotel id.
 *
 * @param {string} arrivalDate - Arrival Date(YYYY-MM-DD)
 * @param {string} departureDate - Departure Date(YYYY-MM-DD)
 * @param {Number} hotelId - Hotel ID
 * @returns => returns array of Room details
 */
async function getPropertyRoomDetailsFromBookingAPi(arrivalDate, departureDate, hotelId){
    try{
        const { data } = await instance.get('properties/v2/get-rooms', {
            params: {
                hotel_id: hotelId,
                departure_date: departureDate,
                arrival_date: arrivalDate,
                rec_guest_qty: '2',
                rec_room_qty: '1',
                currency_code: 'USD',
                languagecode: 'en-us',
                units: 'imperial'
            }
        });
        return data;
    } catch (error) {
        if (error.response) {
            console.error(`Third-party api error >> ${JSON.stringify(error.response.data)}`);
            throw new Error('Something went wrong');
        } else if (error.request) {
            console.error(`Axios error >> ${JSON.stringify(error.request)}`);
            throw new Error('Something went wrong');
        } else {
            console.error(`Axios error >> ${JSON.stringify(error.message)}`);
            throw new Error('Something went wrong');
        }
    }
}

module.exports = { getPropertyListFromBookingApi, getPropertyDescriptionFromBookingApi, getPropertyRoomDetailsFromBookingAPi };